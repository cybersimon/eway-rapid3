import os
from setuptools import setup, find_packages

PYPI_RESTRUCTURED_TEXT_INFO = ""

setup(
    name = 'eway-rapid3',
    version = '0.0.1',
    packages = find_packages(),

    install_requires = [
        'requests==1.0.4',
        ],
    package_data = {'eway-rapid3': []},
    entry_points = {},

    # metadata for upload to PyPI
    author = 'Simon Johnson',
    author_email = 'simon@sjit.com.au',
    description = 'Python wrapper for the eWAY Rapid 3.0 API',
    license = '',
    platforms=['any'],
    keywords = 'eWAY Rapid 3.0 API',
    url = 'https://bitbucket.org/cybersimon/eway-rapid3',
    long_description = PYPI_RESTRUCTURED_TEXT_INFO,
    classifiers = [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        ],
)
