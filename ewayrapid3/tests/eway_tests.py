from unittest import TestCase
import unittest
from ewayrapid3 import API
import Customer, ShippingAddress, Item, Option, Payment


class TestEway(TestCase):

    def test_create_access_code(self):
        AUTH = {
            'key': 'F9802CtO8HuzuIGR4BBB6s90AljgosrAWgHi10uaxzKYcE1AWhM70rdt80uZqhVapCewe2',
            'secret': 'Dardlet4',
        }
        eway = API(**AUTH)
        customer_data = {
            "Reference": "A12345",
            "Title": "Mr.",
            "FirstName": "John",
            "LastName": "Smith",
            "CompanyName": "Demo Shop 123",
            "JobDescription": "Developer",
            "Street1": "Level 5",
            "Street2": "369 Queen Street",
            "City": "Auckland",
            "State": "",
            "PostalCode": "1010",
            "Country": "nz",
            "Email": "sales@demoshop123.com",
            "Phone": "09 889 0986",
            "Mobile": "09 889 0986"
        }

        customer = Customer(customer_data, None)

        shipping_address_data = {
            "ShippingMethod": "NextDay",
            "FirstName": "John",
            "LastName": "Smith",
            "Street1": "Level 5",
            "Street2": "369 Queen Street",
            "City": "Auckland",
            "State": "",
            "Country": "nz",
            "PostalCode": "1010",
            "Email": "sales@demoshop123.com",
            "Phone": "09 889 0986"
        },

        shipping_address = ShippingAddress(shipping_address_data, None)

        items = []
        items.add(
            Item({
                "SKU": "SKU1",
                "Description": "Description1"
            },
        ))

        options = []
        options.add(
            Option({
                "Value": "Option1" },
             )
        )

        payment_data = {
            "TotalAmount": 100.05,
            "InvoiceNumber": "Inv 21540",
            "InvoiceDescription": "Individual Invoice Description",
            "InvoiceReference": "513456",
            "CurrencyCode": "AUD"
        }

        payment = Payment(payment_data, None)

        access_code = eway.create_access_code(
            customer, shipping_address, items, options, payment)
        print access_code
        self.assertIsNotNone(access_code)


def main():
    unittest.main()

if __name__ == "__main__":
    main()